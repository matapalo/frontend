import NavBar from "./public/js/components/navbar";
import SlotView from "./public/js/components/slotview";
import LoginView from "./public/js/views/LoginView";
import HomeView from "./public/js/views/HomeView";
import FeedbackView from "./public/js/views/FeedbackView";
import RegisterView from "./public/js/views/RegisterView";
import UnauthView from "./public/js/views/UnauthView";
import IndexView from "./public/js/views/IndexView";

import "./public/css/style.css";

import AuthService from "./public/js/services/authService";
import AboutView from "./public/js/views/AboutView";

window.onbeforeunload = event => {
  event.preventDefault();
  return "";
};

window.onload = () => {
  const authService = new AuthService();
  if (authService.getID() !== null) {
    console.log("logged in");
    authService.removeID();
  }
  const { pathname, origin } = window.location;
  window.location.replace(`${origin}${pathname}#index`);
  document.querySelector("nav-bar").setAttribute("activeview", "index");
};

document.addEventListener("error-event", evt => {
  const { detail } = evt;
  const { status, text } = detail;
  let msgelem = document.querySelector("header > p#error_msg");
  console.log("element", msgelem);
  msgelem.hidden = false;
  msgelem.textContent = text;
  msgelem.style.color = "var(--danger-color)";
  setTimeout(() => {
    document.querySelector("header > p#error_msg").hidden = true;
  }, 10000);
});

customElements.define("nav-bar", NavBar);
customElements.define("nav-slot", SlotView);
customElements.define("login-view", LoginView);
customElements.define("home-view", HomeView);
customElements.define("feedback-view", FeedbackView);
customElements.define("register-view", RegisterView);
customElements.define("unauthorized-view", UnauthView);
customElements.define("index-view", IndexView);
customElements.define("about-view", AboutView);
