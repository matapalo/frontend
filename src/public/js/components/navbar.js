import AuthService from "../services/authService";
import EventHandler from "../services/eventHandler";
import { firstCharToUpperCase } from "../utils/funcs";
import logger from "../utils/logger";

export default class NavBar extends HTMLElement {
  static get observedAttributes() {
    return ["activeview"];
  }

  constructor() {
    super();
    this.activeView = null;
    this.activeLink = null;
    this.authService = new AuthService();
    this.eventHandler = new EventHandler();
  }

  connectedCallback() {
    this.activeLinkClass = this.getAttribute("activelinkclass");
    if (!this.activeLinkClass) this.activeLinkClass = "active-link";

    // alustus, käyttäjä ei kirjautuneena --> parametriksi "out"
    this.innerHTML = this.navbar("out");

    this.querySelectorAll("a").forEach(e => {
      e.onclick = event => this.registerLink(event);
    });

    document.addEventListener("account-event", event => this.logon(event));
  }

  registerLink(event) {
    const target = event.target;
    this.onLinkClicked({ target });
  }

  onLinkClicked(evt) {
    const { target } = evt;
    if (this.activeLink) {
      this.activeLink.classList.toggle(this.activeLinkClass);
    }
    this.activeLink = target;
    this.activeLink.classList.toggle(this.activeLinkClass);
    this.activeView = this.activeLink.href.split("#")[1];
    // observer attribute
    this.setAttribute("activeView", this.activeView);
  }

  attributeChangedCallback(name, oldValue, newValue) {
    this.eventHandler.navbarEvent(newValue);
  }

  /**
   * Kirjautumistapahtuma "account-event" kuuntelijasta
   *
   * @param {*} evt
   */
  logon(evt) {
    logger.info("id in session?", this.authService.getID());
    const { detail } = evt;
    const { loggedIn } = detail;
    logger.info("logged in?", loggedIn);
    const view = loggedIn ? "home" : "index";
    this.innerHTML = loggedIn ? this.navbar("in") : this.navbar("out");
    logger.info("hash", window.location.hash);
    logger.info("view", view);
    document.querySelector("nav-bar").setAttribute("activeView", view);

    this.querySelectorAll("a").forEach(e => {
      e.onclick = event => this.registerLink(event);
    });
  }

  in() {
    return { l1: "home", l2: "logout" };
  }

  out() {
    return { l1: "login", l2: "register" };
  }

  /**
   *
   * @param {string} state
   * state --> "out" || "in"
   */
  navbar(state = "out") {
    let { l1, l2 } = state === "out" ? this.out() : this.in();
    let l3 = "about",
      l4 = "feedback";

    // merkkijonon 1. kirjain isoksi kirjaimeksi
    let args = firstCharToUpperCase(l1, l2, l3, l4);

    return `
    <style>
      ul, li, a {
        background-color: var(--secondary)
      }
      @media screen and (min-width: 40em) {
        nav ul {
          height: 100%;
          display: grid;
          grid-template-areas:
            ". ${l1} ."
            ". ${l2} ."
            ". ${l3} ."
            ". ${l4} .";
          grid-template-columns: repeat(3, 1fr);
          grid-template-rows: repeat(4, 1fr);
        }
      }
      @media screen and (max-width: 40em) {
        nav ul {
          height: 100%;
          display: grid;
          grid-template-areas: "${l1} ${l2} ${l3} ${l4}";
          grid-template-columns: repeat(5, 1fr);
          grid-template-rows: 1fr;
        }
      }
    </style>
    <ul style="list-style-type:none;">
      <li id="${l1}"><a href="#${l1}">${args[0]}</a></li>
      <li id="${l2}"><a href="#${l2}">${args[1]}</a></li>
      <li id="${l3}"><a href="#${l3}">${args[2]}</a></li>
      <li id="${l4}"><a href="#${l4}">${args[3]}</a></li>
    </ul>`;
  }
}
