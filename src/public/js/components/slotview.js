"use strict";

import LoginView from "../views/LoginView";
import HomeView from "../views/HomeView";
import FeedbackView from "../views/FeedbackView";
import RegisterView from "../views/RegisterView";
import UnauthView from "../views/UnauthView";
import IndexView from "../views/IndexView";
import AuthService from "../services/authService.js";
import EventHandler from "../services/eventHandler.js";
import AuthView from "../views/AboutView";

export default class SlotView extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.slotView = null;
    this.authService = new AuthService();
    this.eventHandler = new EventHandler();
  }

  connectedCallback() {
    this.shadowRoot.innerHTML = `
        <style>
            slot[name="view"] > * {
              display: flex;
              flex-direction: column;
              align-items: center;
              justify-content: center;
              width: 100%;
              height: 100%;
            }
        </style>
        <slot name="view"></slot>
        `;
    this.slotView = this.shadowRoot.querySelector("[name=view]");
    document.addEventListener("nav-bar", event => this.onNavigation(event));
  }

  onNavigation(evt) {
    const { detail } = evt;
    const { view } = detail;
    // authguard home-sivulle
    const ID = this.authService.getID() || null;

    console.log("detail ", detail);
    let newChild;
    switch (view) {
      case "login":
        newChild = new LoginView();
        break;
      case "home":
        ID === null
          ? (newChild = new UnauthView())
          : (newChild = new HomeView());
        break;
      case "feedback":
        newChild = new FeedbackView();
        break;
      case "register":
        newChild = new RegisterView();
        break;
      case "index":
        newChild = new IndexView();
        break;
      case "logout":
        newChild = new IndexView();
        this.authService.removeID();
        this.eventHandler.accountEvent(false);
        break;
      case "about":
        newChild = new AuthView();
        break;
      default:
        throw new Error(`Unknown route ${view}`);
    }
    this.loadView(newChild);
    window.location.hash = `#${view}`;
  }

  loadView(newChild) {
    if (this.slotView.firstElementChild) {
      this.slotView.replaceChild(newChild, this.slotView.firstElementChild);
    } else {
      this.slotView.appendChild(newChild);
    }
  }
}
