import AuthService from "./authService";
import EventHandler from "./eventHandler";
import logger from "../utils/logger";
// mocha-test library
const fetch = require("node-fetch");

const httpOptions = {
  method: "POST",
  mode: "cors",
  credentials: "same-origin",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*"
  }
};

export default class HttpClient {
  constructor() {
    if (process.env.NODE_ENV !== "test") {
      this.authService = new AuthService();
      this.eventHandler = new EventHandler();
    }
    // env-tiedostoa ei git-repossa --> vaihtoehtona serverin private ip: http://192.168.55.55
    this.url = process.env.API_URL || "http://192.168.55.55";
  }

  async sendFeedback(id, post) {
    logger.info("id", id);
    logger.info("post", post);
    const urlFeedback = `${this.url}/posts`;
    const res = await fetch(urlFeedback, {
      ...httpOptions,
      body: JSON.stringify({ id, post })
    });
    logger.info("res", res);
    if (res.status == 201) {
      logger.info("Feedback sended!");
    } else {
      this.eventHandler.errorEvent(res.status);
    }
  }

  async login(user) {
    const urlLogin = `${this.url}/login`;
    logger.info("url login", urlLogin);
    const res = await fetch(urlLogin, {
      ...httpOptions,
      body: JSON.stringify({ user })
    });
    const resJson = await res.json();
    logger.info("status", res.status);
    logger.info("values", resJson);
    if (res.status == "200" && resJson.message == "Valid credentials") {
      if (process.env.NODE_ENV !== "test") {
        this.authService.setID(resJson.id);
        this.authService.setUsername(user.username);
        this.eventHandler.accountEvent(true);
      }
      return resJson;
    } else {
      if (process.env.NODE_ENV !== "test") {
        this.eventHandler.errorEvent(res.status);
        throw new Error("Unauthorized");
      }
      return resJson;
    }
  }

  async register(user) {
    const urlRegister = `${this.url}/register`;
    const res = await fetch(urlRegister, {
      ...httpOptions,
      body: JSON.stringify({ user })
    });

    const resJson = await res.json();
    if (res.status == 201 && resJson.message == "Account created") {
      logger.info("registration success!");
    } else {
      logger.info("registration failed!");
      if (process.env.NODE_ENV !== "test") {
        this.eventHandler.errorEvent(res.status);
      }
    }
  }
}
