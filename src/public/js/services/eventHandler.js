import logger from "../utils/logger";

export default class EventHandler {
  constructor() {}

  navbarEvent(newValue) {
    const event = new CustomEvent("nav-bar", {
      detail: {
        view: newValue
      },
      bubbles: true
    });
    document.dispatchEvent(event);
  }

  accountEvent(loggedIn) {
    logger.info("account event, logged in?", loggedIn);
    const event = new CustomEvent("account-event", {
      detail: {
        loggedIn
      },
      bubbles: false
    });
    document.dispatchEvent(event);
  }

  errorEvent(statuscode) {
    let errortxt;
    switch (statuscode) {
      case 401:
        errortxt = "Unauthorized";
        break;
      case 404:
        errortxt = "Not found";
        break;
      default:
        errortxt = "Unknown error";
    }
    const event = new CustomEvent("error-event", {
      detail: {
        status: statuscode,
        text: errortxt
      },
      bubbles: false
    });
    document.dispatchEvent(event);
  }
}
