export default class AuthService {
  constructor() {
    if (process.env.NODE_ENV !== "test") {
      this._storage = window.sessionStorage;
    }
  }

  getToken() {
    return this._storage.getItem("token") || "No existing token";
  }

  setToken(token) {
    this._storage.setItem("token", token);
  }

  getID() {
    return this._storage.getItem("id") || null;
  }

  setID(id) {
    this._storage.setItem("id", id);
  }

  removeID() {
    this._storage.removeItem("id");
  }

  getUsername() {
    return this._storage.getItem("username") || null;
  }

  setUsername(username) {
    this._storage.setItem("username", username);
  }
}
