class User {
  constructor() {
    this.email = null;
    this.password = null;
    this.confirm = null;
    this.username = null;
    this.id = 0;
  }
}

module.exports = User;
