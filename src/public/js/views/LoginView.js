"use strict";
import HttpClient from "../services/http-client";
import User from "../models/user";
import { createInput } from "../utils/funcs";
import { isPasswordLongEnough } from "../utils/validator";
import logger from "../utils/logger";

export default class LoginView extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.setAttribute("id", "login-view");
    this.user = new User();
  }

  connectedCallback() {
    // template-elementin kloonaus, mikä on form-elementti
    let template = document.querySelector("#templateForm");
    let templateContent = template.content;
    this.shadowRoot.appendChild(templateContent.cloneNode(true));

    // lisäyksiä template-elementin form-elementtiin
    this.shadowRoot.querySelector("header").textContent = "Otsikko";
    this.shadowRoot.querySelector("label").textContent = "Kirjaudu";
    let elems = this.createElements();
    this.shadowRoot.querySelector("[slot=input]").appendChild(elems);
    this.shadowRoot.addEventListener("submit", event => this.onSubmit(event));
    this.shadowRoot.addEventListener("input", event => this.onInput(event));
  }

  createElements() {
    /**
     *  funktio createInput tiedostosta ../utils/funcs.js
     *  @param type="text"
     *  @param value="username"
     *
     */
    let username = createInput("text", "username");
    let password = createInput("password", "password");

    let span = document.createElement("span");
    span.appendChild(username);
    span.appendChild(password);
    return span;
  }

  onSubmit(evt) {
    evt.preventDefault();
    const httpClient = new HttpClient();
    httpClient.login(this.user);
  }

  onInput(evt) {
    const { target } = evt;
    this.controlInput(target.name, target.value);
    this.controlError(target.name, target.value);
  }

  controlError(target_name, value) {
    const errorControl = {
      username: value => {
        value.match(/^[a-zA-Z0-9]{3,30}$/)
          ? logger.info("true")
          : logger.info("false");
      },
      password: value => {
        isPasswordLongEnough(value)
          ? logger.info("true")
          : logger.info("false");
      }
    };
    errorControl[target_name](value);
  }

  controlInput(target_name, value) {
    const inputControl = {
      username: value => (this.user.username = value),
      password: value => (this.user.password = value)
    };
    inputControl[target_name](value);
    logger.info("input - user:", this.user);
  }
}
