"use strict";
import HttpClient from "../services/http-client";
import AuthService from "../services/authService";
import { createInput } from "../utils/funcs";

export default class FeedbackView extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.authService = new AuthService();
    this.client = new HttpClient();
    this.id = this.authService.getID();
    this.feedback = null;
  }

  connectedCallback() {
    const template = document.getElementById("templateForm");
    const templateContent = template.content;
    this.shadowRoot.appendChild(templateContent.cloneNode(true));
    let elems = this.createElems();
    this.shadowRoot.querySelector("[slot='input']").appendChild(elems);
    this.shadowRoot.addEventListener("submit", event => this.onSubmit(event));
    this.shadowRoot.addEventListener("input", event => this.onInput(event));
  }

  createElems() {
    let name = createInput("text", "name");
    let email = createInput("email", "email");
    let textarea = document.createElement("textarea");
    textarea.setAttribute("rows", "2");
    textarea.setAttribute("cols", "40");
    textarea.setAttribute("id", "feedbackTextarea");
    textarea.setAttribute("placeholder", "Send feedback");

    let span = document.createElement("span");
    span.appendChild(name);
    span.appendChild(email);
    span.appendChild(textarea);
    return span;
  }

  onInput(evt) {
    const { value } = evt.target;
    console.log("val", value);
    this.feedback = value;
  }

  onSubmit(evt) {
    evt.preventDefault();
    console.log("Submit!");
    if (this.feedback !== null && this.feedback !== undefined) {
      this.client.sendFeedback(this.id, this.feedback);
    }
  }
}
