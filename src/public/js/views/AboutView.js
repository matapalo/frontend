export default class AboutView extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
    <style>
        span {
            margin: 1em;
            background-color: var(--primary);
        }
    </style>
    <span>
        <div>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima unde
            voluptates inventore hic autem. Unde doloremque quo eligendi vero. Sequi,
            fuga incidunt expedita aspernatur mollitia blanditiis minima eum?
            Reiciendis, numquam.      
        <div>
    <span>`;
  }
}
