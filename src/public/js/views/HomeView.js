import AuthService from "../services/authService.js";

export default class HomeView extends HTMLElement {
  constructor() {
    super();
    this.authService = new AuthService();
  }

  connectedCallback() {
    let username = this.authService.getUsername();
    let h1 = document.createElement("h1");
    h1.textContent = username ? `Welcome ${username}` : "Home page";
    this.appendChild(h1);
  }
}
