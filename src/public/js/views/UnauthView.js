export default class UnauthView extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    let h1 = document.createElement("h1");
    h1.textContent = "Unauthorized...You need to login!";
    this.appendChild(h1);
  }
}
