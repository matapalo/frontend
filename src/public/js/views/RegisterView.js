import User from "../models/user";
import HttpClient from "../services/http-client";
import {
  MatchingPasswords,
  isPasswordLongEnough,
  isPasswordStrong
} from "../utils/validator";
import { createInput } from "../utils/funcs";

export default class RegisterView extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.setAttribute("id", "register-view");
    this.user = new User();
  }

  connectedCallback() {
    const template = document.querySelector("#templateForm");
    const templateContent = template.content;
    this.shadowRoot.appendChild(templateContent.cloneNode(true));

    this.shadowRoot.querySelector("label").textContent = "Rekisteröinti";
    this.shadowRoot.querySelector("header").textContent = "Otsikko";

    let elems = this.createElements();
    this.shadowRoot.querySelector("[slot=input]").appendChild(elems);
    this.shadowRoot.addEventListener("submit", event => this.onSubmit(event));
    this.shadowRoot.addEventListener("input", event => this.onInput(event));
  }

  createElements() {
    let username = createInput("text", "username");
    let password = createInput("password", "password");
    let confirm = createInput("password", "confirm");
    confirm.setAttribute("placeholder", "confirm password");

    let span = document.createElement("span");
    span.appendChild(username);
    span.appendChild(password);
    span.appendChild(confirm);
    return span;
  }

  onSubmit(evt) {
    evt.preventDefault();
    const httpClient = new HttpClient();
    console.log("this.user", this.user);
    if (!MatchingPasswords(this.user.password, this.user.confirm)) {
      console.error("error", "password error");
      return;
    }
    httpClient.register(this.user);
  }

  onInput(evt) {
    const { target } = evt;
    this.controlInput(target.name, target.value);
  }

  controlInput(target_name, value) {
    const inputControl = {
      username: value => (this.user.username = value),
      password: value => (this.user.password = value),
      confirm: value => (this.user.confirm = value)
    };
    inputControl[target_name](value);
    console.log("input - user:", this.user);
  }
}
