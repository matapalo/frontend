export function firstCharToUpperCase() {
  let args = [];
  for (let i in arguments) {
    let oldValue = arguments[i].toString();
    let newValue = oldValue.charAt(0).toUpperCase() + oldValue.slice(1);
    args.push(newValue);
  }
  return args;
}

/**
 * @param {string} type
 * @param {string} value
 */
export function createInput(type = "text", value) {
  let element = document.createElement("input");
  element.setAttribute("type", type);
  element.setAttribute("id", value);
  element.setAttribute("name", value);
  element.setAttribute("placeholder", value);
  return element;
}
