const isPasswordStrong = password => {
  if (!password) {
    return false;
  }
  return true;
};

const isPasswordLongEnough = password => {
  if (password.length < 5) {
    return false;
  }
  return true;
};

const MatchingPasswords = (password, confirm) => {
  if (password === confirm) {
    return true;
  }
  return false;
};

export { isPasswordStrong, isPasswordLongEnough, MatchingPasswords };
