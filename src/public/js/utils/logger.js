const logger = {
  info: (...params) => {
    if (process.env.NODE_ENV !== "test") {
      console.log(...params);
    }
  },
  error: (...params) => {
    console.log(...params);
  }
};

export default logger;
