const fs = require("fs");

const readFile = () => {
  const data = fs.readFileSync("./src/db/data.json");
  const json = JSON.parse(data);
  return json;
};

const writeFile = data => {
  fs.readFile("./src/db/data.json", (err, fileData) => {
    try {
      const file = JSON.parse(fileData);
      file.users.push(data);
      fs.writeFile("./src/db/data.json", JSON.stringify(file), err => {
        if (err) console.error("Error writing file", err);
        else console.log("Successfully wrote file");
      });
    } catch (e) {
      console.log("WriteFile error", err);
    }
  });
};

const checkCredentials = async loginData => {
  const data = await readFile();
  const { email, password } = loginData;
  try {
    for (let user of data.users) {
      if (user.email == email && user.password == password) {
        console.log("MATCH");
        return true;
      }
    }
  } catch (e) {
    console.error("Error:", e.message);
  }

  return false;
};

const registerUser = data => {
  try {
    writeFile(data);
  } catch (e) {
    console.error("Error:", e.message);
  }
};

export { checkCredentials, registerUser };
