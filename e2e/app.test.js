import { Selector, ClientFunction } from "testcafe";

fixture`starting from index-page`.page`http://192.168.55.55`;

const user = { username: "validi", password: "testi" };

test("Try to register", async t => {
  const loginLink = Selector("#register");
  await t.expect(loginLink.textContent).eql("Register");

  await t.click("#register > a");

  const getLocation = ClientFunction(() => document.location.href.toString());

  await t.expect(getLocation()).contains("/#register");

  const paragraph = shadowRootsOnRegistration();
  // type user credentials
  await t.typeText(paragraph.find("#username"), user.username);
  await t.typeText(paragraph.find("#password"), user.password);
  await t.typeText(paragraph.find("#confirm"), user.password);
  // submit form
  await t.click(paragraph.find("#submitBtn"));

  function shadowRootsOnRegistration() {
    const slotRoot = Selector("#slot_root");

    const paragraph = Selector(
      () => {
        return demoPageSelector().shadowRoot.querySelector("#register-view");
      },
      { dependencies: { demoPageSelector: slotRoot } }
    );

    const paragraph2 = Selector(
      () => {
        return demoPageSelector().shadowRoot.querySelector("form");
      },
      { dependencies: { demoPageSelector: paragraph } }
    );

    return paragraph2;
  }
});

test("Try to login", async t => {
  const loginLink = Selector("#login");
  await t.expect(loginLink.textContent).eql("Login");

  await t.click("#login > a");

  const getLocation = ClientFunction(() => document.location.href.toString());

  await t.expect(getLocation()).contains("/#login");

  const paragraph = shadowRootsOnLogin();
  // type user credentials
  await t.typeText(paragraph.find("#username"), user.username);
  await t.typeText(paragraph.find("#password"), user.password);
  // submit form
  await t.click(paragraph.find("#submitBtn"));

  function shadowRootsOnLogin() {
    const slotRoot = Selector("#slot_root");

    const paragraph = Selector(
      () => {
        return demoPageSelector().shadowRoot.querySelector("#login-view");
      },
      { dependencies: { demoPageSelector: slotRoot } }
    );

    const paragraph2 = Selector(
      () => {
        return demoPageSelector().shadowRoot.querySelector("form");
      },
      { dependencies: { demoPageSelector: paragraph } }
    );

    return paragraph2;
  }
});
