# WEBPACK-projekti

HUOM. keskeneräinen!

## Projektin tarkoitus

Harjoitella front- ja backend ohjelmointia sekä soveltaa opittua DevSecOps-sovelluskehitykseen.

---

## Serveri

Ansible-vagrant -projektissa serverinä node-sovellus ja tietokantana PostgreSQL. Vagrantissa pyörii (Ubuntu Bionic) Node-sovellus ja tietokanta erillään kontitettuina (docker). Virtuaalikone sisältää Haproxy:n ("kuormantasaukseen").

Host-koneelta yhteys yksityisen-verkon (private network) kautta virtuaalikoneeseen. IP-osoite määritellään Vagrantfile-tiedostossa.

## Frontend

Werkkosivuston koodausta ilman frameworkkia tai kirjastoa (testimielessä). Toteutukseen on käytetty Template- ja Custom-elementtejä sekä Shadow DOM:ia.

Tällä hetkellä vain webpack-projektin javascript-tiedostot on "bundlattu" eli luotu yksittäinen koodin sisältämä main-tiedosto.

### Toteutus

- PWA (Progressive Web Application) mobiili- ja web-toteutus
- SPA (Single-Page Application)

## Backend

Backendin toteutukseen Node.js (express-kirjastolla).

## Yksikkötestaus

- Mocha
- Chai

## Integraatiotestaus

Testataan useita sovelluksen komponentteja samanaikaisesti.

- Robot Framework

Integraatiotestaukseen tarvitaan omaa "kirjastoa", joka löytyy python-tiedostona (shadow) lib-kansiosta (./integration_tests/lib).

Projektin frontend-toteutuksessa on käytetty sisäistä alipuuta (Shadow DOM ns. varjo-DOM). Alipuun elementteihin ei pääse käsiksi ilman selenium webdriveria --> oma "kirjasto".
