const path = require("path");
const common = require("./webpack.common");
const merge = require("webpack-merge");
const webpack = require("webpack");

module.exports = merge(common, {
  mode: "development",
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        PRODUCTION: JSON.stringify(false),
        TEST: JSON.stringify(false),
        NODE_ENV: JSON.stringify("no-test")
      }
    })
  ],
  output: {
    filename: "main.js",
    path: path.resolve(__dirname, "dist")
  }
});
