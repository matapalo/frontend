#! python

import selenium
from selenium import webdriver
from robot.libraries.BuiltIn import BuiltIn


chrome_options = webdriver.ChromeOptions()
chrome_options.binary_location = "/usr/bin/google-chrome"
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
driver = webdriver.Chrome(executable_path='/usr/local/bin/chromedriver',chrome_options=chrome_options)
driver.get('http://192.168.55.55')

assert 'Projekti' in driver.title


def expand_shadow_element(element):
    shadow_root = driver.execute_script("return arguments[0].shadowRoot", element)
    return shadow_root


def get_innerHTML(element):
    return driver.execute_script("return arguments[0].innerHTML", element)


def try_to_login(username, password):
    navbar = driver.find_element_by_css_selector("#login > a").click()

    root_1 = driver.find_element_by_tag_name("nav-slot")

    shadow_root_1 = expand_shadow_element(root_1)

    root_2 = shadow_root_1.find_element_by_tag_name("login-view")

    shadow_root_2 = expand_shadow_element(root_2)

    element_1 = shadow_root_2.find_element_by_id("username")
    element_1.send_keys(username)

    element_2 = shadow_root_2.find_element_by_id("password")
    element_2.send_keys(password)

    element_3 = shadow_root_2.find_element_by_id("submitBtn")
    element_3.click()


def try_to_register(username, password, confirm):
    navbar = driver.find_element_by_css_selector("#register > a").click()

    root_1 = driver.find_element_by_tag_name("nav-slot")

    shadow_root_1 = expand_shadow_element(root_1)

    root_2 = shadow_root_1.find_element_by_tag_name("register-view")

    shadow_root_2 = expand_shadow_element(root_2)

    element_1 = shadow_root_2.find_element_by_id("username")
    element_1.send_keys(username)

    element_2 = shadow_root_2.find_element_by_id("password")
    element_2.send_keys(password)

    element_3 = shadow_root_2.find_element_by_id("confirm")
    element_3.send_keys(confirm)

    element_4 = shadow_root_2.find_element_by_id("submitBtn")
    element_4.click()
 

def find_text(elem):
    root = driver.find_element_by_tag_name("nav-slot")
    shadow_root = expand_shadow_element(root)
    element = shadow_root.find_element_by_tag_name(elem)
    return get_innerHTML(element)


def get_driver():
    seleniumlib = BuiltIn().get_library_instance("SeleniumLibrary")
    driver = seleniumlib.driver
    return driver