*** Settings ***

Library      SeleniumLibrary    timeout=2
Library      ../lib/shadow.py

*** Variables ***


${SERVER}      192.168.55.55
${Protocol}    http://
${Browser}     chrome
${MainURL}     ${Protocol}${SERVER}
${HomeURL}     ${MainURL}/#home
${LoginURL}    ${MainURL}/#login
${RegisterURL}  ${MainURL}/#register


*** Keywords ***

Run Keywords
    [Arguments]  ${Delay}
    Maximize Browser Window
    Set Selenium speed     ${Delay}

Navigate to Login
    Open Browser   ${LoginURL}   ${Browser}

Navigate to Home
    Go To  ${HomeURL}

Navigate to Register
    Open Browser  ${RegisterURL}

Login Page Should Be Open
    Click Element   css:#login > a:nth-child(1) 
    Location Should Be   ${LoginURL}

Home Page Should Be Open
    Location Should Be    ${HomeURL}

Register Page Should Be Open
    Click Element   css:#register > a:nth-child(1)
    Location Should Be    ${RegisterURL}

Login Should Work
    [Arguments]  ${USERNAME}  ${PASSWORD}
    Try To Login  ${USERNAME}  ${PASSWORD}
    ${check}    Find Text   h1
    Should Be Equal     ${check}    Welcome ${USERNAME}

Registration Should Work
    [Arguments]  ${USERNAME}  ${PASSWORD}  ${CONFIRM}
    Try To Register  ${USERNAME}  ${PASSWORD}  ${CONFIRM}
