#! /bin/bash

begin(){
    echo "begin"
    case $1 in
        (-l)
            begin="xvfb-run -a robot --loglevel INFO --outputdir results/login/"
        ;;
        (-r)
            begin="xvfb-run -a robot --loglevel DEBUG --outputdir results/register/"
        ;;
        (*)
            echo "Do nothing"
            return
        ;;
    esac
}

middle(){
    echo "middle"
    case $1 in
        (-l)
            middle="--variable USERNAME:user --variable PASSWORD:testi" 
        ;;
        (-r)
            middle="--variable USERNAME:user --variable PASSWORD:testi  --variable CONFIRM:testi"
        ;;
        (*)
            echo "Do nothing"
            return
        ;;
    esac
}

end(){
    echo "end"
    case $1 in
        (-l)
            end="tests/login.robot"
        ;;
        (-r)
            end="tests/register.robot"
        ;;
        (*)
            echo "Do nothing"
            return
        ;;
    esac
}

run(){
    case $1 in
    (-login)
        begin -l
        middle -l 
        end -l    
    ;;
    (-register)
        begin -r
        middle -r
        end -r
    ;;
    (*)
        echo "Do nothing"
        return
    ;;
    esac
    echo "$begin $middle $end"
    eval "$begin $middle $end"
}

usage(){
    echo \
    "
        LOGIN
        $ run -login 
        -----
        REGISTER
        $ run -register
    "
}