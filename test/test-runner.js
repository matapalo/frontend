const { utilTests } = require("./test-specs/component-spec.js");
const { httpClientLoginTest } = require("./test-specs/http-client-spec.js");
import { testInputValidator } from "./test-specs/validator-spec";
require("./utils/dom.js");

process.env.NODE_ENV = "test";

utilTests();
testInputValidator();
httpClientLoginTest();
