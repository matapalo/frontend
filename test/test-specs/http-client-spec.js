const chai = require("chai");
const expect = chai.expect,
  should = chai.should();

import HttpClient from "../../src/public/js/services/http-client.js";

const correct = { username: "user", password: "testi" };
const incorrect = { username: "invalid", password: "testi" };
const invalidPassword = { username: "invalid", password: "test" };

const httpClientLoginTest = () => {
  describe("FrontEnd Http-client for login", () => {
    before(async () => {
      const httpClient = new HttpClient();
      //await httpClient.clearDB();
      await httpClient.register(correct);
    });
    it("Should return an id for the user", async () => {
      const httpClient = new HttpClient();
      const result = await httpClient.login(correct);
      expect(result).to.be.a("object");
      expect(result).to.have.all.keys("id", "message");
      expect(result.message).to.equal("Valid credentials");
    });
    it("Should return status 401 and message Unauthorized", async () => {
      const httpClient = new HttpClient();
      const result = await httpClient.login(incorrect);
      expect(result).to.be.a("object");
      expect(result).to.have.all.keys("message");
      expect(result.message).to.equal("Unauthorized");
    });
    it("Should return status 422 and errors array", async () => {
      const httpClient = new HttpClient();
      const result = await httpClient.login(invalidPassword);
      expect(result).to.be.a("object");
      expect(result).to.have.all.keys("errors");
      expect(result.errors[0]).to.have.all.keys(
        "location",
        "param",
        "value",
        "msg"
      );
      expect(result.errors[0].msg).to.contain(
        "Password must be at least 5 characters in length"
      );
    });
  });
};

module.exports = { httpClientLoginTest };
