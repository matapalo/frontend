"use strict";

const chai = require("chai");
const expect = chai.expect,
  should = chai.should();

import {
  isPasswordStrong,
  isPasswordLongEnough,
  MatchingPasswords
} from "../../src/public/js/utils/validator";

export const testInputValidator = () => {
  describe("Is password strong", function() {
    it("Should return false for empty string password", () => {
      const password = "";
      expect(isPasswordStrong(password)).to.equal(false);
    });
    it("Should return false for too short password", () => {
      const password = "test";
      password.should.be.a("string");
      expect(isPasswordLongEnough(password)).to.equal(false);
    });
    it("Should return true for long enough password", () => {
      const password = "testi";
      password.should.be.a("string");
      expect(isPasswordLongEnough(password)).to.equal(true);
    });
  });

  describe("Confirm password", () => {
    it("Should not match and return false", () => {
      const password = "testi",
        confirm = "";
      expect(MatchingPasswords(password, confirm)).to.equal(false);
    });
    it("Should match and return true", () => {
      const password = "testi",
        confirm = password;
      expect(MatchingPasswords(password, confirm)).to.equal(true);
    });
  });
};
