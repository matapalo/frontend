const chai = require("chai"),
  expect = chai.expect;

import { firstCharToUpperCase } from "../../src/public/js/utils/funcs.js";

function utilTests() {
  describe("util-function firstCharToUpperCase", () => {
    it("Should change first character of a string to upper case", () => {
      let oldValue = "testi";
      let newValue = firstCharToUpperCase(oldValue);
      expect(...newValue).to.equal("Testi");
    });
  });
}

module.exports = { utilTests };
