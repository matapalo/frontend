var HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const Dotenv = require("dotenv-webpack");
const path = require("path");

module.exports = {
  entry: ["@babel/polyfill", "./src/app.js"],
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/template.html"
    }),
    new Dotenv({
      path: ".env",
      safe: false
    }),
    new webpack.DefinePlugin({
      "process.env": {
        API_URL: process.env.API_URL
      }
    })
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          "style-loader", // 2. styles into DOM
          "css-loader" // 1. turns css into commonjs
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: "babel-loader"
      }
    ]
  }
};
